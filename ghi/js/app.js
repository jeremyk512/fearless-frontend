function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-title"><small class="text-muted">${location}</small></p>
          <p class="card-text">${description}</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">${new Date(startDate).toLocaleDateString()} - ${new Date(endDate).toLocaleDateString() }</li>
      </ul>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.error(`Error: ${response.status} - ${response.statusText}`);
      } else {
        const data = await response.json();

        const columns = document.querySelectorAll('.col-sm-3'); // Use Bootstrap column class

        for (let i = 0; i < data.conferences.length; i++) {
          const conference = data.conferences[i];
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);

            // Append the card to the appropriate column
            const columnIndex = i % columns.length;
            columns[columnIndex].innerHTML += html;
          }
        }
      }
    } catch (error) {
        console.error('error', error);
    }

  });

