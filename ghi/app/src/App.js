import Nav from './Nav';
import React from 'react';

function App(props) {
  return (
    <>
      <Nav />
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Conference</th>
            </tr>
          </thead>
          <tbody>
            {Array.isArray(props.attendees) && props.attendees.length > 0 ? (
              props.attendees.map((attendee) => (
                <tr key={attendee.id} className="table-light">
                  <td>{attendee.name}</td>
                  <td>{attendee.conference}</td>
                </tr>
              ))
            ) : (
              <tr>
                <td colSpan="2">No attendees available.</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default App;
